local wezterm = require 'wezterm'

return {
  font = wezterm.font 'MesloLGS NF',
  font_size = 13,
  bold_brightens_ansi_colors = true,
  color_scheme = "Abernathy",
  hide_tab_bar_if_only_one_tab = true,
  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  },
}
